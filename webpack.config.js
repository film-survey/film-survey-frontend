const HtmlWebPackPlugin = require("html-webpack-plugin");
const ErrorOverlayPlugin = require("error-overlay-webpack-plugin");
const path = require("path");
const CopyWebpackPlugin = require('copy-webpack-plugin');


module.exports = {
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                },
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader",
                    },
                ],
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.scss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    "style-loader",
                    // Translates CSS into CommonJS
                    {
                        loader: "css-loader",
                        options: { url: false },
                    },
                    // 'css-loader',
                    // Compiles Sass to CSS
                    {
                        loader: "sass-loader",
                        options: {
                            additionalData: `$resourceUrl: '${ process.env.RESOURCES_URL  }';`
                        }
                    }
                ],
            },
            {
                test: /\.(woff(2)?|ttf|eot|otf)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[name].[ext]",
                            outputPath: "fonts/",
                        },
                    },
                ],
            },
            {
                test: /\.svg$/,
                use: ["@svgr/webpack"],
            },
        ],
    },
    devServer: {
        historyApiFallback: true,
        hot: true,
        https: true,
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./public/index.html",
            filename: "./index.html",
            publicPath: "/",
        }),
        new ErrorOverlayPlugin(),
        new CopyWebpackPlugin({
            patterns: [
                { from: 'public/assets', to: 'assets',
                    noErrorOnMissing: true
                },
            ]
        })
    ],
    resolve: {
        modules: [path.resolve("./src"), path.resolve("./node_modules")],
    },
    devtool: "cheap-module-source-map", // 'eval' is not supported by error-overlay-webpack-plugin
};
