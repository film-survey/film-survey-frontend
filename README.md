## FILM SURVEY 
### Frontend

### Project setup
- Install project dependencies by running `yarn` or `npm install`.
- Run project in local development server with `yarn start` or `npm run start`.
- (default: https://localhost:8080/)

### Production build
- Build project in production mode with `yarn build` or `npm run build`.

### Fix code
- Lint script and stylesheets by running `yarn lint` or `yarn run lint`.