// Attachment 4 - Example response for 500 POST / GET/api/v1/survey
export const error500Response = {
    errors: [
        {
            title: "Internal Server Error",
            detail: "Something went wrong. We're working on it!",
        },
    ],
};
