import { Survey } from "../Domain/Survey";
import { SurveyAttributes } from "../Domain/SurveyAttributes";
import { Question } from "../Domain/Question";

export class SurveyBuilder {
    mapToDomain(data) {
        return new Survey(data.type, data.id, this.mapAttributesToDomain(data.attributes));
    }

    mapAttributesToDomain(data) {
        return new SurveyAttributes(
            data.title,
            data.description,
            data.questions.map((q) => this.mapQuestionToDomain(q))
        );
    }

    mapQuestionToDomain(data) {
        return new Question(
            data.questionId,
            data.questionType,
            data.label,
            data.required,
            data.attributes ? data.attributes : null
        );
    }
}
