import { getSurvey200Response } from "./mock-data/getSurvey200Response";
import { error500Response } from "./mock-data/error500Response";
import { SurveyBuilder } from "./SurveyBuilder";
import { BaseRepository } from "../../Common/BaseRepository";

export class SurveyRepository extends BaseRepository {
    constructor() {
        super();
        this.builder = new SurveyBuilder();
    }

    getSurvey() {
        try {
            const response = getSurvey200Response; // to be replaced with api call

            return this.builder.mapToDomain(response.data); // to be replaced with real api response
        } catch (e) {
            return error500Response;
        }
    }

    postSurveyAnswers(answers) {
        this.storage.removeItem("lastSurvey");
        const payload = {
            data: {
                type: "surveyAnswers",
                attributes: {
                    answers: answers,
                },
            },
        };

        try {
            // submit to backend to be added
            this.storage.setItem("lastSurvey", JSON.stringify(payload)); // mock storage that would be DB with localStorage
            return true;
        } catch (e) {
            this.storage.removeItem("lastSurvey");
            // error handling to be added
            return error500Response;
        }
    }
}
