export class Question {
    constructor(questionId, questionType, label, required, attributes) {
        this.questionId = questionId;
        this.questionType = questionType;
        this.label = label;
        this.required = required;
        this.attributes = attributes;
    }

    getQuestionId() {
        return this.questionId;
    }

    getQuestionType() {
        return this.questionType;
    }

    getLabel() {
        return this.label;
    }

    isRequired() {
        return this.required;
    }

    getAttributes() {
        return this.attributes;
    }
}
