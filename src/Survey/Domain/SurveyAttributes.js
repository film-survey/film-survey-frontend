export class SurveyAttributes {
    constructor(title, description, questions) {
        this.title = title;
        this.description = description;
        this.questions = questions;
    }

    getTitle() {
        return this.title;
    }

    getDescription() {
        return this.description;
    }

    getQuestions() {
        return this.questions;
    }
}
