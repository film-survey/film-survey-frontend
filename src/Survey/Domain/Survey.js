export class Survey {
    constructor(type, id, attributes) {
        this.type = type;
        this.id = id;
        this.attributes = attributes;
    }

    getType() {
        return this.type;
    }

    getId() {
        return this.id;
    }

    getAttributes() {
        return this.attributes;
    }
}
