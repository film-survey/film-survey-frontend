import React, { useEffect, useState } from "react";
import { Form, Input, Rate } from "antd";
import { TYPE_RATING, TYPE_TEXT } from "./InputType";

const mapTypeToInputType = (type) => {
    switch (type) {
        case TYPE_TEXT:
            return "text";
        case TYPE_RATING:
            return "rating";
        default:
            return type;
    }
};

const getStandardInput = (label, type, value, handleInputChange, errorMessage, inputProperties = {}) => {
    const { help: helpMessage = "", ...properties } = inputProperties;
    const help = errorMessage === null ? helpMessage || null : errorMessage;
    const validationStatus = errorMessage === null ? "success" : "error";

    return (
        <Form.Item label={label} validateStatus={validationStatus} help={help} preserve={false}>
            <Input type={mapTypeToInputType(type)} onChange={handleInputChange} {...properties} value={value} />
        </Form.Item>
    );
};

const getRatingInput = (label, type, value, handleInputChange, errorMessage, inputOptions = {}) => {
    const validationStatus = errorMessage === null ? "success" : "error";

    return (
        <Form.Item label={label} validateStatus={validationStatus} help={errorMessage} preserve={false}>
            <Rate count={inputOptions.count ? inputOptions.count : 5} onChange={handleInputChange} />
        </Form.Item>
    );
};

/**
 *
 * @param type
 * @param label
 * @param initialValue
 * @param validator - (value) => Promise<true|string> needs to return true if valid, string message if invalid
 * @returns {[inputValue, ReactComponent, isValid]}
 */
const useFormItem = ({ type, label, value, inputOptions = {}, validator = null }) => {
    const [inputValue, updateInputValue] = useState(value);
    const [error, setError] = useState(null);
    const [hasError, setHasError] = useState(false);

    useEffect(() => {
        updateInputValue(value);
    }, [value]);

    const runValidation = (newValue = inputValue, withStateChange = true) => {
        return new Promise((resolve) => {
            if (validator === null) {
                if (withStateChange) {
                    setError(null);
                }
                setHasError(false);
                resolve(true);
                return;
            }

            (async () => {
                const validatorValue = await validator(newValue);

                // If valid
                if (validatorValue === true) {
                    if (withStateChange) {
                        setError(null);
                    }
                    setHasError(false);
                    resolve(true);
                    return;
                }

                if (withStateChange) {
                    setError(validatorValue);
                }
                setHasError(true);
                resolve(false);
            })();
        });
    };

    useEffect(() => {
        runValidation(value, false);
    }, []);

    const handleInputChange = (e) => {
        let newValue;
        switch (type) {
            case TYPE_RATING:
                newValue = e;
                break;
            default:
                newValue = e.target.value;
                break;
        }

        // run validation
        if (validator !== null) {
            (async () => {
                const validatorValue = await validator(newValue);
                setError(validatorValue === true ? null : validatorValue);
                setHasError(validatorValue !== true);
            })();
        } else {
            setError(null);
            setHasError(false);
        }

        updateInputValue(newValue);
    };

    let component;

    switch (type) {
        case TYPE_RATING:
            component = getRatingInput(label, type, inputValue, handleInputChange, error, inputOptions);
            break;
        default:
            component = getStandardInput(label, type, inputValue, handleInputChange, error, inputOptions);
            break;
    }

    const handleSetError = (newError) => {
        // to be used when errors return from API
        setError(newError);
        setHasError(newError !== null);
    };

    const handleSetValue = (newValue) => {
        // for default values
        updateInputValue(newValue);
        runValidation(newValue, false);
    };

    return [inputValue, component, !hasError, handleSetValue, handleSetError, runValidation];
};

export default useFormItem;
