import React, { useState } from "react";
import HTMLReactParser from "html-react-parser";
import PropTypes from "prop-types";
import { Button, notification } from "antd";
import useFormItem from "../Helpers/useFormItem";
import { TYPE_RATING } from "../Helpers/InputType";

export const DynamicFormComponent = ({ title, description, questions, handleSubmit }) => {
    const validations = [];
    const inputCache = [];
    const [notificationVisible, setNotificationVisible] = useState(false);
    const getFieldByType = (question) => {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const [value, component, , , , runValidation] = useFormItem({
            type: question.getQuestionType(),
            label: question.getLabel(),
            inputOptions: {
                count: question.getQuestionType() === TYPE_RATING ? question.getAttributes().max : undefined,
            },
            validator: (v) => {
                if (question.isRequired()) {
                    if (v === undefined || v === null || v === "") {
                        return "This field is required";
                    }
                    if (question.getQuestionType() === TYPE_RATING && v === 0) {
                        return "This field is required";
                    }
                }
                return true;
            },
        });
        validations.push(runValidation);
        inputCache.push({
            questionId: question.getQuestionId(),
            answer: value,
        });

        return component;
    };

    const checkErrors = async () => {
        const errors = await Promise.all(validations.map((promise) => promise()));
        return !errors.every((value) => value === true);
    };

    const onSubmit = async () => {
        const hasErrors = await checkErrors();

        if (hasErrors) {
            if (!notificationVisible) {
                setNotificationVisible(true);
                notification.error({
                    message: "Please correct invalid fields",
                    placement: "bottomRight",
                    duration: 3,
                });
                setTimeout(() => {
                    setNotificationVisible(false);
                }, 3000);
            }
            return;
        }

        handleSubmit(inputCache);
    };

    return (
        <form className="dynamic-form">
            <h1>{title}</h1>
            {description && <div className="description">{HTMLReactParser(description)}</div>}
            {questions.map((q, idx) => {
                return (
                    <div className="question" key={idx}>
                        {getFieldByType(q)}
                    </div>
                );
            })}
            <Button onClick={() => onSubmit()}>Submit</Button>
        </form>
    );
};

DynamicFormComponent.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    questions: PropTypes.array.isRequired,
    handleSubmit: PropTypes.func.isRequired,
};

DynamicFormComponent.defaultProps = {
    description: "",
};
