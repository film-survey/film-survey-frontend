import React from "react";
import { Spin } from "antd";
import { DynamicFormComponent } from "./Components/DynamicFormComponent";
import { SurveyRepository } from "../Infrastructure/SurveyRepository";

class MainLayoutControllerComponent extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            loading: true,
        };

        this.repository = new SurveyRepository();
    }

    componentDidMount() {
        this.fetchData();
        this.setState({ loading: false });
    }

    fetchData() {
        this.survey = this.repository.getSurvey();
        this.questions = this.survey.getAttributes().getQuestions();
    }

    async submitForm(answers) {
        await this.repository.postSurveyAnswers(answers);

        // eslint-disable-next-line no-undef
        window.location.href = "/thank-you";
    }

    render() {
        if (this.state.loading) {
            return (
                <div id="page-container">
                    <Spin />
                </div>
            );
        }

        return (
            <div id="page-container">
                <DynamicFormComponent
                    title={this.survey.getAttributes().getTitle()}
                    description={this.survey.getAttributes().getDescription()}
                    questions={this.questions}
                    handleSubmit={(payload) => this.submitForm(payload)}
                />
            </div>
        );
    }
}

export default MainLayoutControllerComponent;
