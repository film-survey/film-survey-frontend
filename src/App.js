import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "antd";
import "./static/style/style.scss";
import MainLayoutControllerComponent from "./Survey/Application/MainLayoutControllerComponent";
import ThankYouPageController from "./ThankYou/Application/ThankYouPageController";

const App = () => {
    return (
        <Router>
            <Routes>
                <Route exact path="*" element={<MainLayoutControllerComponent />} />
                <Route exact path="/thank-you" element={<ThankYouPageController />} />
            </Routes>
        </Router>
    );
};

export default App;
