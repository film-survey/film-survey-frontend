import React from "react";
import { Link } from "react-router-dom";
import { Spin } from "antd";
import { SurveyPreviewRepository } from "../Infrastructure/SurveyPreviewRepository";
import { SurveyRepository } from "../../Survey/Infrastructure/SurveyRepository";

class ThankYouPageController extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            loading: true,
        };

        this.surveyRepository = new SurveyRepository();
        this.previewRepository = new SurveyPreviewRepository();
    }

    componentDidMount() {
        this.fetchData();
        this.setState({ loading: false });
    }

    fetchData() {
        this.survey = this.surveyRepository.getSurvey();
        this.questions = this.survey.getAttributes().getQuestions();
        this.preview = this.previewRepository.getLastSubmittedSurvey();
    }

    render() {
        if (this.state.loading) {
            return (
                <div id="page-container">
                    <Spin />
                </div>
            );
        }

        return (
            <div id="page-container">
                <form className="dynamic-form">
                    <h1>Thanks for submitting your review</h1>
                    <p>Here&apos;s the review you submitted</p>
                    {this.questions.map((q, idx) => {
                        return (
                            <div key={idx} className="preview-container">
                                <p className="question">{q.getLabel()}</p>
                                <p className="answer">
                                    {this.preview.map((a) => {
                                        if (a.getQuestionId() !== q.getQuestionId()) {
                                            return "";
                                        }

                                        if (a.getAnswer() === null) {
                                            return "No answer";
                                        }

                                        return a.getAnswer();
                                    })}
                                </p>
                            </div>
                        );
                    })}
                    <Link to="/">Go back</Link>
                </form>
            </div>
        );
    }
}

export default ThankYouPageController;
