export class Answer {
    constructor(questionId, answer) {
        this.questionId = questionId;
        this.answer = answer;
    }

    getQuestionId() {
        return this.questionId;
    }

    getAnswer() {
        return this.answer;
    }
}
