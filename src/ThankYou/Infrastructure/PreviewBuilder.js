import { Answer } from "../Domain/Answer";

export class PreviewBuilder {
    extractSurveyAnswers(data) {
        return data.attributes.answers.map((a) => this.mapAnswer(a));
    }

    mapAnswer(data) {
        return new Answer(data.questionId, data.answer ? data.answer : null);
    }
}
