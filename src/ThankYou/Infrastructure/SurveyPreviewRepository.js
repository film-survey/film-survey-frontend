import { BaseRepository } from "../../Common/BaseRepository";
import { PreviewBuilder } from "./PreviewBuilder";

export class SurveyPreviewRepository extends BaseRepository {
    constructor() {
        super();

        this.builder = new PreviewBuilder();
    }

    getLastSubmittedSurvey() {
        const response = JSON.parse(this.storage.getItem("lastSurvey")); // retrieve last survey from mock storage
        return this.builder.extractSurveyAnswers(response.data);
    }
}
